#include "Core/Render/Renderer/Renderer.h"
#include "Core/Render/Window/Window.h"
#include "Core/Math/Math.h"
#include "Core/Types/CoreTypes.h"

#include "Polygon.h"

#include "QuadrangleSpawner.h"

#include <vector>
#include <iostream>

int main(int argc, char** argv)
{
	constexpr int WindowWidth = 800;
	constexpr int WindowHeight = 700;
	Window Window(WindowWidth, WindowHeight, "ZPKG_Lab1");

	Renderer Renderer(&Window);

	QuadrangleSpawner Spawner(4, WindowWidth, WindowHeight);

	Timer Timer;
	float DeltaSeconds;
	int FrameRate;

	Renderer.Prepare();
	while (Renderer.IsRunning())
	{
		// Calculate elapsed time from previous tick
		DeltaSeconds = Timer.GetElapsedTime();
		Timer.RegisterCurrentTime();

		FrameRate = DeltaSeconds < 0.001 ? 999 : 1.0 / DeltaSeconds;

		Spawner.Update(DeltaSeconds);

		for (const Polygon& Polygon : Spawner.GetPolygons())
		{
			Renderer.AddToDraw(Polygon.DrawInfo);
			Renderer.AddToDraw(Polygon.DrawTextInfo);
		}

		Renderer.AddToDraw(
			DrawTextInfo
			{
				FrameRate >= 60 ? Color::Green : FrameRate < 30 ? Color::Red : Color(.8, .7, 0),
				Vector3d(0, WindowHeight - 11),
				std::to_string(FrameRate)
			}
		);

		Renderer.Update();
	}
}