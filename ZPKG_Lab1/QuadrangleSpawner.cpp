#include "QuadrangleSpawner.h"

#include "Core/Math/Math.h"

QuadrangleSpawner::QuadrangleSpawner(int QuadranglesNum, int MaxX, int MaxY)
{
	for (int i = 0; i < QuadranglesNum; ++i)
	{
		Vector3d RandPosition
		{
			Math::RandRange(MaxOuterCircleRadius, MaxX - MaxOuterCircleRadius),
			Math::RandRange(MaxOuterCircleRadius, MaxY - MaxOuterCircleRadius)
		};

		Polygons.emplace_back(Polygon::GenerateRandomPolygon(
			RandPosition, 
			4, 
			Math::RandRange(MinOuterCircleRadius, MaxOuterCircleRadius)));
	}

	StartTimer.RegisterCurrentTime();

	NextRemoveTime = TimeToFirstRemove;
}

void QuadrangleSpawner::Update(float DeltaSeconds)
{
	if (StartTimer.GetElapsedTime() < NextRemoveTime || Polygons.empty())
	{
		return;
	}

	NextRemoveTime += RemoveRate;

	Polygons.pop_back();
}
