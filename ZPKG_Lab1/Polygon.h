#pragma once

#include "Core/Render/DrawInfo/DrawInfo.h"
#include "Core/Types/CoreTypes.h"

#include <vector>

class Polygon
{
public:
	/** 
	 * Generates random polygon with AnglesNum angles and center at SpawnPosition 
	 * CircleRadius represents radius of outer circle
	 */
	static Polygon GenerateRandomPolygon(const Vector3d& SpawnPosition, int AnglesNum, float CircleRadius);

private:
	/** Calculates AnglesNum random ascending angles from 0 to 360 */
	static std::vector<float> GetRandomAngles(int AnglesNum);

public:
	DrawInfo DrawInfo;

	DrawTextInfo DrawTextInfo;
};

