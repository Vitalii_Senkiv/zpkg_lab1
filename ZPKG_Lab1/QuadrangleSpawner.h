#pragma once

#include "Polygon.h"

#include "Core/Types/Timer/Timer.h"

#include <vector>

class QuadrangleSpawner
{
public:
	QuadrangleSpawner(int QuadranglesNum, int MaxX, int MaxY);

public:
	void Update(float DeltaSeconds);

	const std::vector<Polygon>& GetPolygons() { return Polygons; }

private:
	float TimeToFirstRemove = 3.f;
	float RemoveRate = 2.f;

	float NextRemoveTime = 0;

	float MinOuterCircleRadius = 90;
	float MaxOuterCircleRadius = 100;

	std::vector<Polygon> Polygons;

	Timer StartTimer;
};

