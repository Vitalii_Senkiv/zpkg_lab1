#include "Polygon.h"

#include <cassert>
#include <sstream>
#include <iomanip>

#include "Core/Math/Math.h"

constexpr float FullCircleAngle = 360.f;

Polygon Polygon::GenerateRandomPolygon(const Vector3d& SpawnPosition, int AnglesNum, float CircleRadius)
{
	assert(AnglesNum > 2 && "AnglesNum is less than 3!");

	const float Step = FullCircleAngle / AnglesNum;

	Polygon Polygon;

	// Add random vertices
	for (float Angle : GetRandomAngles(AnglesNum))
	{
		const Vector3d Vertex
		{
			SpawnPosition.X + Math::SinDeg(Angle) * CircleRadius,
			SpawnPosition.Y + Math::CosDeg(Angle) * CircleRadius
		};

		Polygon.DrawInfo.Vertices.emplace_back(Vertex);
	}

	Color RandColor = Color::MakeRandomColor();
	Polygon.DrawInfo.Color = RandColor;

	Polygon.DrawTextInfo.Color = RandColor.GetInversed();
	Polygon.DrawTextInfo.Position = SpawnPosition;
	Polygon.DrawTextInfo.Text = RandColor.GetHexCode();

	return Polygon;
}

std::vector<float> Polygon::GetRandomAngles(int AnglesNum)
{
	std::vector<float> Angles;

	int Sum = 0;
	for (int i = 0; i < AnglesNum; ++i)
	{
		int RandCoeficient = Math::RandRange(1, 3);
		Sum += RandCoeficient;

		Angles.push_back(RandCoeficient);
	}

	float AccumulatedNum = 0;
	for (float& Angle : Angles)
	{
		AccumulatedNum += Angle;

		Angle = AccumulatedNum / static_cast<float>(Sum) * FullCircleAngle;
	}

	return Angles;
}
